Willkommen auf dem Repo unseres Zwischenprojektes bei der AW Academy. 
Beteiligt waren Heinrich Böse und Danies Schnietz.
Dies war unser Zwischenprojekt und wurde von uns bis zu den Backlog punkten 7 und 9 umgesetzt.

!!Da hier eine Datenbank von den Dozenten gestellt wurde sind keine Anzeigen hier aufrufbar.!!

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


### Product Backlog
1. Als Besucher der Website kann ich eine neue Anzeige erstellen. Eine Anzeige besteht aus folgenden Informationen:
    o Titel, maximal 80 Zeichen lang
    o Beschreibungstext
    o Mein Name
    o Ort (z.B. München)
    o Optional: ein Preis in Euro, kann auch 0 sein.
    o Falls die Anzeige einen Preis hat, kann dieser als „Verhandlungsbasis“ gekennzeichnet werden
    o Kontaktmöglichkeit: E-Mail-Adresse und/oder Telefonnummer
2. Als Besucher der Website sehe ich eine Liste der neuesten Anzeigen. Die Liste zeigt maximal 20 Einträge auf einmal. Es soll für jede Anzeige der Titel sowie die ersten 100 Zeichen des Beschreibungstextes angezeigt werden.
3. Als Besucher der Website kann ich eine Anzeige in der Liste anklicken, um mehr dazu zu erfahren. Ich bekomme alle Informationen der Anzeige (gemäß Story 1) auf einer eigenen Seite angezeigt. Außerdem kann ich dort sehen, wie alt die Anzeige ist.
4. Als Besucher der Website kann ich einen Ort eingeben und erhalte eine Liste der neuesten Anzeigen aus diesem Ort. Die Liste soll wie bei Story 2 gestaltet sein.
5. Als Besucher der Website kann ich mir mit Name, E-Mail-Adresse und Passwort ein Benutzerkonto Registrieren
6. Als Besucher kann ich mich in mein Benutzerkonto einloggen.
7. Als eingeloggter Benutzer kann ich Anzeigen erstellen, ohne Name und E-Mail-Adresse angeben zu müssen.
8. Als eingeloggter Benutzer kann ich eine Liste meiner Anzeigen sehen und die Details dazu abrufen
9. Als Besucher der Website kann ich einen Suchbegriff eingeben und erhalte eine Liste mit Anzeigen, bei welchen der Suchbegriff im Titel oder Beschreibungstext vorkommt.
10. Als eingeloggter Benutzer kann ich Anzeigen zu einer persönlichen Merkliste hinzufügen.
11. Als eingeloggter Benutzer kann ich Anzeigen von meiner Merkliste entfernen
12. Als eingeloggter Benutzer erkenne ich in jeder Liste und Detail-Ansicht, ob die Anzeige in meiner Merkliste steht und kann sie direkt hinzufügen/entfernen
13. Als eingeloggter Benutzer kann ich meine Anzeigen bearbeiten
14. Als eingeloggter Benutzer kann ich meine Anzeigen löschen. Versehentliches Löschen sollte vermieden werden, z.B. mit einer zusätzlichen Bestätigung
15. Als eingeloggter Benutzer kann ich Nachrichten zu einer Anzeige an den Benutzer schicken, welcher die Anzeige erstellt hat
16. Als eingeloggter Benutzer kann ich Nachrichten zu meinen Anzeigen empfangen und beantworten


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify




   limit: 20,
   offset: 0,
   where: {
     and: [
       {location: "Berlin"},
       {
          or: [
            {title: {regexp: "/bike/i"}},
            {description: {regexp: "/bike/i"}},
          ]
       },
     ]
  },
}