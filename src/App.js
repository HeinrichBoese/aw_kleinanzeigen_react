import React, { useState, useEffect } from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  // NavLink,
} from "react-router-dom";
import Container from "@material-ui/core/Container";
import Details from "./components/Details";
import Register from "./components/Register";
import Login from "./components/Login";
import { InputForm } from "./components/InputForm";
import List from "./components/List";
import MenuAppBar from "./components/AppBar";
import Error404 from "./components/Error404";
import AOK from "./components/AOK";

function App() {
  const [loggedIn, setLoggedIn] = useState(false);
  const [refreshUserToggle, setRefreshUserToggle] = useState(false);
  const [userInfo, setUserInfo] = useState({});

  const logout = () => {
    setLoggedIn(false);
    setUserInfo({});
    sessionStorage.clear();
  };

  useEffect(() => {
    const token = sessionStorage.getItem("jwt");
    const getUserData = async () => {
      let response = await fetch(
        "https://awacademy-kleinanzeigen.azurewebsites.net/user/me",
        {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      );
      if (response.status !== 200) {
        logout();
      } else {
        setLoggedIn(true);
        let data = await response.json();
        setUserInfo(data);
      }
    };
    if (token !== null && token !== "undefined") {
      getUserData();
    }
  }, [refreshUserToggle]);

  return (
    <Router>
      <MenuAppBar {...userInfo} loggedIn={loggedIn} logout={logout} />
      <Container maxWidth="lg">
        <div className="App">
          <Switch>
            <Route exact path="/">
              <List />
            </Route>
            <Route exact path="/InputForm">
              <InputForm {...userInfo} loggedIn={loggedIn} />
            </Route>
            <Route path="/details/:id">
              <Details />
            </Route>
            <Route path="/AOK">
              <AOK />
            </Route>
            <Route exact path="/register">
              <Register loggedIn={loggedIn} />
            </Route>
            <Route exact path="/login">
              <Login
                loggedIn={loggedIn}
                setRefreshUserToggle={setRefreshUserToggle}
                refreshUserToggle={refreshUserToggle}
              />
            </Route>
            <Route path="*">
              <Error404 />
            </Route>
          </Switch>
        </div>
      </Container>
    </Router>
  );
}

export default App;
