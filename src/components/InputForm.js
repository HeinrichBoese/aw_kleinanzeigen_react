import React from "react";
import { useHistory } from "react-router-dom";
import  { useFormik } from "formik";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";
import {
  Box,
  Button,
  Container,
  FormGroup,
  FormControlLabel,
  Grid,
  TextField,
  Paper,
  Switch,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    alignItems: "flex-start",
    padding: "30px 10px",
  },
  textField: {
    padding: "3px 3px",
    width: "100%",
  },
  button: {
    width: "100%",
  },
}));
const GreenSwitch = withStyles({
  switchBase: {
    color: green[200],
    "&$checked": {
      color: green[500],
    },
    "&$checked + $track": {
      backgroundColor: green[500],
    }
  },
  checked: {},
  track: {},
})(Switch);

const corPrice = (p) => {
  let outP = p.replace(/[,]/, ".");
  return Number.parseFloat("0"+outP);
};
const validate = (values) => {
  const errors = {};
  if (!values.name) {
    errors.name = "The Name is Required";
  } else if (values.name.length < 3) {
    errors.name = "Must be 3 characters or more";
  }
  if (!values.title) {
    errors.title = "The Title is Required";
  } else if (values.title.length < 10 || values.title.length > 80) {
    errors.title = "Must be at least 10 characters and a max of 80 characters";
  }
  if (!values.description) {
    errors.description = "The Description is Required";
  } else if (
    values.description.length < 10 ||
    values.description.length > 500) {
    errors.description =
      "Must be at least 10 characters and a max of 500 characters";
  }
  if (!values.location) {
    errors.location = "The Location is Required";
  } else if (values.location.length < 3) {
    errors.location = "Must be at least 3 characters long";
  }
  if (/[^0-9.,]/.test(values.price)) {
    errors.price = "bitte nur Zahlen eingeben";
  } else if (/[.|,]\w*[.|,]/.test(values.price)) {
    errors.price = "bitte nur einen Punkt oder Komma verwenden";
  }
  if (/^[^0|+]/.test(values.phone)) {
    errors.phone = 'Bitte mit einer "0" oder einem "+" beginnen';
  } else if (/[^0-9-+ ]/.test(values.phone)) {
    errors.phone =
      'bitte nur Zahlen eingeben und ggf. mit "-" oder " " trennen';
  } else if (/.[+]/.test(values.phone)) {
    errors.phone = 'bitte das "+" nur am Anfang der Nummer verwenden';
  }
  if (!values.email) {
    errors.email = "E-Mail is Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Invalid email address";
  }
  return errors;
};

export const InputForm = (props) => {
  const history = useHistory();
  const classes = useStyles();
  const formik = useFormik({
    initialValues: {
      title: "",
      description: "",
      name: props.loggedIn ? props.name : "",
      location: "",
      price: "",
      priceNegotiable: false,
      phone: "",
      email: props.loggedIn ? props.email : "",
    },
    validate,
    validateOnMount: true,
    onSubmit: async (values) => {      
      formik.values.price = corPrice(formik.values.price);
      let response = await fetch("https://awacademy-kleinanzeigen.azurewebsites.net/ad", {
        method: "post",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(values),
      });
      if (response.status !== 200) {
        alert("Etwas ging schief. Probiere es nochmal und Prüfe die Daten");
      } else {
        history.push("/AOK")
      }

    }, // End onSubmit .then(() => history.push("/"));
  });
  const formikProps = (name, initialValue = "") => ({
    id: name,
    name: name,
    variant: "outlined",
    value: formik.values[name],
    onChange: formik.handleChange,
    onBlur: formik.handleBlur,
    error: Boolean(formik.errors[name] && formik.touched[name]),
    className: classes.textField,
  });

  return (
    <Container maxWidth="sm">
      <h1>Neue Anzeige</h1>
      <Box >
        <Paper elevation={2} className={classes.root}>
          <form onSubmit={formik.handleSubmit}>
            <Grid
              container
              spacing={2}
              direction="row"
              justify="center"
              alignItems="flex-start"
            >
              <Grid item xs={12}>
                <Box px={2} py={1}>
                  <TextField
                    {...formikProps("title")}
                    type="text"
                    label={
                      formik.touched.title && formik.errors.title
                        ? formik.errors.title
                        : "Titel mit max. " +
                        formik.values.title.length +
                        "/80 Zeichen"
                    }
                  />
                </Box>
              </Grid>
              <Grid item xs={12}>
                <Box px={2} py={1}>
                  <TextField
                    {...formikProps("description")}
                    type="textarea"
                    multiline
                    rows={4}
                    label={
                      formik.touched.description && formik.errors.description
                        ? formik.errors.description
                        : "Bitte eine Beschreibung ihres Angebotes eingeben. " +
                        formik.values.description.length +
                        "/500 Zeichen"
                    }
                  />
                </Box>
              </Grid>
              <Grid item xs={12}>
                <Box px={2} py={1}>
                  <TextField
                    {...formikProps("name")}
                    type="text"
                    label={
                      formik.touched.name && formik.errors.name
                        ? formik.errors.name
                        : "ihr Rufname"
                    }
                    disabled={props.loggedIn}
                  />
                </Box>
              </Grid>
              <Grid item xs={12}>
                <Box px={2} py={1}>
                  <TextField
                    {...formikProps("location")}
                    type="text"
                    label={
                      formik.touched.location && formik.errors.location
                        ? formik.errors.location
                        : "ihre Stadt"
                    }
                  />
                </Box>
              </Grid>
              <Grid item xs={12}>
                <Box px={2} py={1}>
                  <FormGroup>
                    <FormControlLabel
                      control={
                        <GreenSwitch
                          checked={formik.values.priceNegotiable}
                          onChange={formik.handleChange}
                        />
                      }
                      label={
                        formik.values.priceNegotiable
                          ? "Anzeige auf Verhandlungsbasis:"
                          : "Anzeige mit Festpreis:"
                      }
                      id="priceNegotiable"
                      name="priceNegotiable"
                      value={formik.values.priceNegotiable}
                    />
                    <Box py={1}>
                      <TextField
                        {...formikProps("price")}
                        type="text"
                        label={
                          formik.touched.price && formik.errors.price
                            ? formik.errors.price
                            : "Preis"
                        }
                      />
                    </Box>
                  </FormGroup>
                </Box>
              </Grid>
              <Grid item xs={12}>
                <Box px={2} py={1}>
                  <TextField
                    {...formikProps("phone")}
                    type="text"
                    label={
                      formik.touched.phone && formik.errors.phone
                        ? formik.errors.phone
                        : "Telefon Nummer"
                    }
                  />
                </Box>
              </Grid>
              <Grid item xs={12}>
                <Box px={2} py={1}>
                  <TextField
                    {...formikProps("email")}
                    type="email"
                    label={
                      formik.touched.email && formik.errors.email
                        ? formik.errors.email
                        : "E-Mail Adresse"
                    }
                    disabled={props.loggedIn}
                  />
                </Box>
              </Grid>
              <Grid item xs={8}>
                <Box px={2} py={1}>
                  <Button
                    className={classes.button}
                    type="submit"
                    variant="outlined"
                    color="primary"
                    disabled={formik.isValidating || formik.isSubmitting}
                  >
                    Submit
                  </Button>
                </Box>

              </Grid>
            </Grid>
          </form>
        </Paper>
      </Box>
    </Container>
  );
};
