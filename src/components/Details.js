import React from "react";
import useFetch from "react-fetch-hook";
import { useParams, Redirect } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import {
  Container,
  Grid,
  Paper,
} from "@material-ui/core";

const calcDays = (dateStr) => {
  const now = new Date();
  const backThen = new Date(dateStr);
  return parseInt((now - backThen) / (24 * 3600 * 1000));
};
const toDigit = (price) => {
  return Number.parseFloat(price).toFixed(2);
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: '75%',
  },
  paper: {
    padding: theme.spacing(2),
    margin: 'auto',
    maxWidth: '100%',
  },
  image: {
      width: "100%",
      height: "100%",
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
    borderRadius: '5px',
  },
}));

export default function Details(props) {


  const classes = useStyles();

  let { id } = useParams();
  let { isLoading, data } = useFetch(
    "https://awacademy-kleinanzeigen.azurewebsites.net/ad/" + id
  );
  if (!isLoading && !data){return(<Redirect to="/404" />)}else
  return (
    <Container className={classes.root}>
      <h1>Detailansicht</h1>
      <Paper className={classes.paper}>
        {!isLoading && (
          <Grid container spacing={2}>
            <Grid item xs={3}>
              <ButtonBase className={classes.image}>
                <img className={classes.img} alt="complex" src="https://loremflickr.com/320/240" />
              </ButtonBase>
            </Grid>
            <Grid item xs={8} sm container spacing={2}>
              <Grid item xs container direction="column" spacing={2}>
                <Grid item xs>
                  <Typography gutterBottom variant="subtitle1">
                    {data.title}
                  </Typography>
                  <Typography variant="body2" gutterBottom>
                    {data.description}
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography variant="body2" color="textSecondary">
                    {data.name ? `Name: ${data.name}`:"Name fehlt"} {data.location?` aus ${data.location}`:"Ort fehlt"}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                    {data.email ? `Email: ${data.email}`:"Keine E-Mail"} {data.phone && "Telefon: "+data.phone}
                  </Typography>
                </Grid>
              </Grid>
              <Grid item xs={3} container direction="row" spacing={2}>
                <Grid item >
                  <Typography variant="subtitle1">{data.price ? data.priceNegotiable ? "Verhand"+"lungs"+"basis:" : "Festpreis:" : "Preis:"}</Typography>
                  <Typography variant="subtitle1">{data.price ? `${toDigit(data.price)} €` :  "Keine Angabe"}</Typography>
                </Grid>
                <Grid container alignContent="flex-end" spacing={2} >
                  <Grid item>
                  <Typography variant="subtitle1" color="textSecondary">
                    {calcDays(data.createdAt)===0?"Erstellt Heute":
                    calcDays(data.createdAt)===1?"Erstellt Gestern":
                    `Erstellt vor ${calcDays(data.createdAt)} Tagen`}
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        )}
      </Paper>
    </Container>
  );
}

