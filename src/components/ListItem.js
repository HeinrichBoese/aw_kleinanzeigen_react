import React from "react";
import { Link } from "react-router-dom";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import { styled, makeStyles } from '@material-ui/core/styles';

const MyLink = styled(Link)({
  textDecoration: 'none',
    '&:focus, &:hover, &:visited, &:link, &:active': {
        textDecoration: 'none'
    }
});
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: 'auto',
  },
}));

export default function ListItem(props) {
  const classes = useStyles();
  return (
    <div>
      <Box py={1} className={classes.root}>
        <MyLink to={"/details/"+props.id}>
          <Paper className={classes.paper}>
            <Box px={2} py={0.5}>
              <h3>{props.title}</h3>
              <p>{props.description}</p>
            </Box>
          </Paper>
        </MyLink>
      </Box>
    </div>
  );
}
