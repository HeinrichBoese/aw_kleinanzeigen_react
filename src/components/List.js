import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { TextField, Button, Grid, Box } from "@material-ui/core";
import ListItem from "./ListItem";

const useStyles = makeStyles((theme) => ({
  textField: {
    width: "100%",
  },
}));

export default function List(props) {
  const classes = useStyles();

  const [location, setLocation] = useState("");
  const [listData, setListData] = useState(null);

  useEffect(() => {
    const filter = { limit: 20 };
    if (location !== "") {
      filter.where = {
        location: { like: "^" + location + ".*", options: "i" },
      };
    }
    const filterParam = encodeURIComponent(JSON.stringify(filter));
    fetch(
      "https://awacademy-kleinanzeigen.azurewebsites.net/ad/?filter=" +
        filterParam
    )
      .then((r) => r.json())
      .then((ads) => setListData(ads));
  }, [location]);

  return (
    <Box mt={3}>
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <Box my={1} mx={1}>
            <TextField
              id="location"
              value={location}
              onChange={(e) => setLocation(e.target.value)}
              label="Nach Ort filtern"
              variant="filled"
              autoComplete="off"
              className={classes.textField}
            />
            <Button onClick={() => setLocation("") }>
              Reset Filter
            </Button>
          </Box>
        </Grid>
        <Grid item xs={9}>
          {listData &&
            listData.map((ad) => (
              <ListItem
                key={ad.id}
                title={ad.title}
                id={ad.id}
                description={ad.description.slice(0, 100)}
              />
            ))}
        </Grid>
      </Grid>
    </Box>
  );
}
