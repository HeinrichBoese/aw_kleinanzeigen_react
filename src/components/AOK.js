import React, {  useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import { useHistory } from "react-router-dom";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    marginTop: theme.spacing(2),
    margin: "auto",
    maxWidth: "75%",
  },

}));


export default function AOK(props) {
  const classes = useStyles();
  const history = useHistory();

  useEffect(() => {
    setTimeout(()=> history.push("/"), 3000 )
    });

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <h1>Daten Erfolgreich verschickt</h1>
      </Paper>
    </div>
  );
}
